import Auth from './auth'
import Link from 'next/link'

function Header({user}) {
  return (
    <header>
      <div>&nbsp;</div>
      {
        user ? (<Auth user={user}/>) : (
          <span>
            <Link href="/login">Login</Link>
            <span>&nbsp;</span>
            <Link href="/signup">Signup</Link>
          </span>
        )
      }
    </header>
  )
}

export default Header