import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie'
import Link from 'next/link'
import { getUser } from '../service/userService';

function Auth({user}) {
  const [state, setState] = useState({
    currentuser: user,
    email: '',
  });

  useEffect(() => {
    async function fetchUserData() {
      const params = {id: Number(user)}
      const result = await getUser(params);
      const {email} = result;
      if (!email) {
        Cookies.remove("currentuser");
        window.location.replace("/login");
      } else {
        setState({
          ...state,
          email,
        });
      }
    };

    fetchUserData();
  }, []);

  const handleLogout = (e) => {
    e.preventDefault();
    Cookies.remove("currentuser");
    window.location.replace("/login");
  };
  return (
    <span suppressHydrationWarning>
      Welcome <Link href="/profile" suppressHydrationWarning className="margin-right"><a suppressHydrationWarning>{state.email}</a></Link>
      <span>&nbsp;</span><a onClick={(e) => handleLogout(e)} className="margin-left" suppressHydrationWarning>Logout</a>
    </span>
  );
}

export default Auth