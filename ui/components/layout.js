import Header from './header'
import Footer from './footer'

function Layout({children, user}) {
  return (
    <>
      <Header user={user} />
      <main>
        {children}
      </main>
      <Footer/>
    </>
  )
}

export default Layout