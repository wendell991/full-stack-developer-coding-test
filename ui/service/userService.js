const USER_SERVICE_URL = `https://localhost:5001/api/users`;

const headers = {
  'Content-Type': 'application/json',
};

const fetchpost = async (params, url) => {
  const result = await fetch(url, {
    method: 'POST',
    headers,
    body: JSON.stringify(params),
  });
  return result.json();
};

export const getUser = async params => {
  return fetchpost(params, `${USER_SERVICE_URL}/getuser`);
};

export const authenticate = async params => {
  return fetchpost(params, `${USER_SERVICE_URL}/authenticate`);
};

export const register = async params => {
  return fetchpost(params, `${USER_SERVICE_URL}`);
};

export const addDetails = async params => {
  return fetchpost(params, `${USER_SERVICE_URL}/adddetail`);
};

export const getDetails = params => {
  return fetchpost(params, `${USER_SERVICE_URL}/userdetails`);
};

export const addAddress = async params => {
  return fetchpost(params,`${USER_SERVICE_URL}/addaddress`);
};

export const getAddress = async params => {
  return fetchpost(params,`${USER_SERVICE_URL}/useraddress`);
};