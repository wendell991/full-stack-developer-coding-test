import React from 'react'
import App from 'next/app'
import Cookies from 'js-cookie'
import './styles.css'
import Layout from '../components/layout'

class MyApp extends App {
  constructor(props){
    super(props);
    const user = Cookies.get('currentuser');
    this.state = {
      user,
    }
  }
  render() {
    const { Component, pageProps } = this.props;
    const { user } = this.state;
    return (
      <Layout user={user}>
        <Component {...pageProps} />
      </Layout>
    )
  }
}

export default MyApp