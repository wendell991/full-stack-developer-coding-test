import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import { register, authenticate } from '../service/userService';

function SignUpPage() {
  const router = useRouter();
  const [state, setState] = useState({
    email: '',
    password: '',
    errorMessage: '',
    isLoading: false,
  });

  const onInputChange = (event) => {
    const { name, value } = event.target;
    setState({
      ...state,
      [name]: value,
    });
  };

  const handleLogin = (result) => {
    const { id } = result;
    Cookies.set("currentuser", id);
    window.location.replace("/profile");
  };

  const onAuthenticate = async () => {
    const { email, password } = state;
    setState({
      ...state,
      isLoading: true,
    });
    const params = {email, password};
    try {
      const result = await authenticate(params);
      handleLogin(result);
    } catch {
      setState({
        ...state,
        isLoading: false,
        errorMessage: 'Login Failed',
      });
    }
  };

  const onSignUp = async (e) => {
    e.preventDefault();
    const { email, password } = state;
    if (!email || !password) {
      setState({
        ...state,
        errorMessage: 'Please enter email and password',
      });
    }
    setState({
      ...state,
      isLoading: true,
    });
    const params = {email, password};
    try {
      const result = await register(params);
      await onAuthenticate();
    } catch {
      setState({
        ...state,
        isLoading: false,
        errorMessage: 'Login Failed',
      });
    }
  };

  return (
    <form onSubmit={onSignUp}>
      <div>
        <span className="margin-right">Email</span>
        <input type="text" name="email" placeholder="email" onChange={onInputChange} />
      </div>
      <div>
        <span className="margin-right">Password</span>
        <input type="password" name="password" placeholder="password" onChange={onInputChange}/>
      </div>
      <div>
        <span>{state.errorMessage}</span>
      </div>
      <div>
        <button className="margin-right" type="submit">Register</button>
        <Link href="/">Cancel</Link>
      </div>
    </form>
  );
}

export default SignUpPage