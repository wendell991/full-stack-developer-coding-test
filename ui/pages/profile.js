import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie'
import { getDetails, getAddress, addDetails, addAddress } from '../service/userService';
import Link from 'next/link'

function ProfilePage() {
  const [state, setState] = useState({
    userid: 0,
    firstName: '',
    lastName: '',
    billingAddress: '',
    billingCity: '',
    billingPostalCode: null,
    billingCountry: '',
    deliveryAddress: '',
    deliveryCity: '',
    deliveryPostalCode: null,
    deliveryCountry: '',
    errorMessage: '',
    isLoading: false,
  });

  useEffect(() => {
    async function fetchUserData() {
      const user = Cookies.get('currentuser');
      const id = Number(user)
      const params = {id };
      const resultDetails = await getDetails(params);
      let firstName = "";
      let lastName = "";
      let billingAddress = "";
      let billingCity = "";
      let billingPostalCode = "";
      let billingCountry = "";
      let deliveryAddress = "";
      let deliveryCity = "";
      let deliveryPostalCode = "";
      let deliveryCountry = "";
      if (resultDetails) {
        firstName = resultDetails.firstName;
        lastName = resultDetails.lastName;
      }

      const resultAddress = await getAddress(params);
      try {
        const resultBilling = resultAddress.filter(function(element){
          return element.type === "Billing"
        })[0];
  
        const resultDelivery = resultAddress.filter(function(element){
          return element.type === "Delivery"
        })[0];
        
        if (resultBilling) {
          billingAddress = resultBilling.addressText;
          billingCity = resultBilling.city;
          billingPostalCode = resultBilling.postalCode;
          billingCountry = resultBilling.country;
        }
  
        if (resultDelivery) {
          deliveryAddress = resultDelivery.addressText;
          deliveryCity = resultDelivery.city;
          deliveryPostalCode = resultDelivery.postalCode;
          deliveryCountry = resultDelivery.country;
        }
        
      }catch{}

      setState({
        ...state,
        userid: id,
        firstName,
        lastName,
        billingAddress,
        billingCity,
        billingPostalCode,
        billingCountry,
        deliveryAddress,
        deliveryCity,
        deliveryPostalCode,
        deliveryCountry,
      });
    };

    fetchUserData();
  }, []);

  const onInputChange = (event) => {
    const { name, value } = event.target;
    setState({
      ...state,
      [name]: value,
    });
  };

  const onSave = async (e) => {
    e.preventDefault();
    const {
      userid,
      firstName,
      lastName,
      billingAddress,
      billingCity,
      billingPostalCode,
      billingCountry,
      deliveryAddress,
      deliveryCity,
      deliveryPostalCode,
      deliveryCountry,
    } = state;
    const paramDetails = {
      userid,
      firstName,
      lastName,
    };

    const paramBilling = {
      userid,
      AddressText: billingAddress,
      City: billingCity,
      PostalCode: Number(billingPostalCode),
      Country: billingCountry,
      Type: 'Billing',
    };

    const paramDelivery = {
      userid,
      AddressText: deliveryAddress,
      City: deliveryCity,
      PostalCode: Number(deliveryPostalCode),
      Country: deliveryCountry,
      Type: 'Delivery',
    };

    try {
      const reponseDetails = await addDetails(paramDetails);
      const reponseBilling = await addAddress(paramBilling);
      const reponseDelivery = await addAddress(paramDelivery);
    } catch {
      setState({
        ...state,
        isLoading: false,
        errorMessage: 'Update Failed',
      });
    }
  };

  return (
    <form onSubmit={onSave}>
      <div>
        <span className="margin-right">First Name:</span>
        <input type="text" name="firstName" placeholder="First Name" onChange={onInputChange} value={state.firstName || ''} />
      </div>
      <div>
        <span className="margin-right">Last Name:</span>
        <input type="text" name="lastName" placeholder="Last Name" onChange={onInputChange} value={state.lastName || ''} />
      </div>
      <div>
      <div>
        <span>Billing Address</span>
        <div>
          <div>
            <span className="margin-right">Address:</span>
            <input type="text" name="billingAddress" placeholder="Address" onChange={onInputChange} value={state.billingAddress || ''} />
          </div>
          <div>
            <span className="margin-right">City:</span>
            <input type="text" name="billingCity" placeholder="City" onChange={onInputChange} value={state.billingCity || ''} />
          </div>
          <div>
            <span className="margin-right">Postal Code:</span>
            <input type="number" name="billingPostalCode" placeholder="Postal Code" onChange={onInputChange} value={state.billingPostalCode || ''} />
          </div>
          <div>
            <span className="margin-right">Country:</span>
            <input type="text" name="billingCountry" placeholder="Country" onChange={onInputChange} value={state.billingCountry || ''} />
          </div>
        </div>
      </div>
      <div>
        <span>Delivery Address</span>
        <div>
          <div>
            <span className="margin-right">Address:</span>
            <input type="text" name="deliveryAddress" placeholder="Address" onChange={onInputChange} value={state.deliveryAddress || ''} />
          </div>
          <div>
            <span className="margin-right">City:</span>
            <input type="text" name="deliveryCity" placeholder="City" onChange={onInputChange} value={state.deliveryCity || ''} />
          </div>
          <div>
            <span className="margin-right">Postal Code:</span>
            <input type="number" name="deliveryPostalCode" placeholder="Postal Code" onChange={onInputChange} value={state.deliveryPostalCode || ''} />
          </div>
          <div>
            <span className="margin-right">Country:</span>
            <input type="text" name="deliveryCountry" placeholder="Country" onChange={onInputChange} value={state.deliveryCountry || ''} />
          </div>
        </div>
      </div>
      <div>
        <span>{state.errorMessage}</span>
      </div>
      <button className="margin-right">Save</button>
      <Link href="/">Cancel</Link>
      </div>
    </form>
  )
}

export default ProfilePage