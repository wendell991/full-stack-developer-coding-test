import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import { authenticate } from '../service/userService';

function LoginPage() {
  const router = useRouter();
  const [state, setState] = useState({
    email: '',
    password: '',
    errorMessage: '',
    isLoading: false,
  });

  const handleLogin = (result) => {
    setState({
      ...state,
      isLoading: true,
    });
    const { id } = result;
    if (!id) {
      setState({
        ...state,
        errorMessage: 'Login Failed',
      });
    } else {
      Cookies.set("currentuser", id);
      window.location.replace("/profile");
    }
  };

  const onAuthenticate = async (e) => {
    e.preventDefault();
    const { email, password } = state;
    if (!email || !password) {
      setState({
        ...state,
        errorMessage: 'Please enter email and password',
      });
    }
    setState({
      ...state,
      isLoading: true,
    });
    const params = {email, password};
    try {
      const result = await authenticate(params);
      handleLogin(result);
    } catch {
      setState({
        ...state,
        isLoading: false,
        errorMessage: 'Login Failed',
      });
    }

  };

  const onInputChange = (event) => {
    const { name, value } = event.target;
    setState({
      ...state,
      [name]: value,
    });
  };
  return (
    <form onSubmit={onAuthenticate}>
      <div>
        <span className="margin-right">Email</span>
        <input type="text" name="email" placeholder="email" onChange={onInputChange} />
      </div>
      <div>
        <span className="margin-right">Password</span>
        <input type="password" name="password" placeholder="password" onChange={onInputChange}/>
      </div>
      <div>
        <span>{state.errorMessage}</span>
      </div>
      <div>
        <button className="margin-right" type="submit">Submit</button>
        <Link href="/">Cancel</Link>
      </div>
    </form>
  );
}

export default LoginPage