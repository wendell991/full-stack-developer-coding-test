﻿namespace examAPI.Models
{
    public class Address
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string AddressText { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string Country { get; set; }
        public string Type { get; set; } //Billing or Delivery
    }
}
