﻿namespace examAPI.Models
{
    public class UserDetails
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
