﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using examAPI.Models;

namespace examAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserContext _context;

        public UsersController(UserContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(long id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost("getuser")]
        public async Task<ActionResult<User>> GetUser(GetUserDetails getUser)
        {
            var user = await _context.Users.FindAsync(getUser.Id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost("authenticate")]
        public async Task<ActionResult<User>> GetUser(Login login)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == login.Email && u.Password == login.Password);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost("userdetails")]
        public async Task<ActionResult<UserDetails>> GetUserDetails(GetUserDetails getUser)
        {
            var userDetails = await _context.UserDetails.FindAsync(getUser.Id);

            if(userDetails == null)
            {
                return NotFound();
            }

            return userDetails;
        }

        [HttpPost("useraddress")]
        public async Task<ActionResult<IEnumerable<Address>>> GetUserAddress(GetUserDetails getUser)
        {
            var userAddress = await _context.Addresses.Where(a => a.UserId == getUser.Id).ToListAsync();

            if(userAddress.Count <= 0)
            {
                return NotFound();
            }

            return userAddress;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(long id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        [HttpPost("adddetail")]
        public async Task<ActionResult<UserDetails>> AddDetail(UserDetails userDetails)
        {
            if (!UserExists(userDetails.UserId))
            {
                return NotFound();
            }

            if (_context.UserDetails.Any(ud => ud.UserId == userDetails.UserId))
            {
                var foundUserDetails = await _context.UserDetails.FirstOrDefaultAsync(ud => ud.UserId == userDetails.UserId);
                foundUserDetails.FirstName = userDetails.FirstName;
                foundUserDetails.LastName = userDetails.LastName;
                _context.Entry(foundUserDetails).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }

                return CreatedAtAction("GetUserDetails", new { id = userDetails.UserId }, userDetails);
            }

            _context.UserDetails.Add(userDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserDetails", new { id = userDetails.UserId }, userDetails);
        }

        [HttpPost("addaddress")]
        public async Task<ActionResult<Address>> AddAddress(Address userAddress)
        {
            if (!UserExists(userAddress.UserId))
            {
                return NotFound();
            }

            if(_context.Addresses.Any(a=> a.UserId == userAddress.UserId && a.Type == userAddress.Type))
            {
                var foundUserAddress = await _context.Addresses.FirstOrDefaultAsync(a => a.UserId == userAddress.UserId && a.Type == userAddress.Type);
                foundUserAddress.AddressText = userAddress.AddressText;
                foundUserAddress.City = userAddress.City;
                foundUserAddress.Country = userAddress.Country;
                foundUserAddress.PostalCode = userAddress.PostalCode;
                _context.Entry(foundUserAddress).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return BadRequest();
                }

                return CreatedAtAction("GetUserAddress", new { id = userAddress.UserId }, userAddress);
            }

            _context.Addresses.Add(userAddress);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserAddress", new { id = userAddress.UserId }, userAddress);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(long id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var userDetails = await _context.UserDetails.FindAsync(id);
            if(userDetails != null)
            {
                _context.UserDetails.Remove(userDetails);
            }

            var userAddress = await _context.Addresses.Where(u => u.UserId == id).ToListAsync();
            if(userAddress.Count > 0)
            {
                _context.Addresses.RemoveRange(userAddress);
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(long id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        private bool UserEmailExists(string email)
        {
            return _context.Users.Any(u => u.Email == email);
        }
    }
}
