# FULL-STACK DEVELOPER CODING TEST

Coding exam for flexisource

In-order to run the whole application the ***API*** needs to be open first.

## API
The API is found in the **api** folder just open the examAPI.sln (Visual studio 2017 or higher should be able to "I use 2017 Community Edition")
Then just run it. Make sure selected profile is ***examAPI***.

The API uses Microsoft.EntityFrameworkCore.InMemory (Nugget Package) so the data are only retain while the application is running.

if the generated api is not in https://localhost:5001
please update the UI and set the value of the variable USER_SERVICE_URL in the userService.js file
please make sure you update it correctly as the api needs to start on /api/users like: https://localhost:5001/api/users

-------
## UI
For the UI you will need npm installed not sure what versions are applicable but I use npm version ***6.13.4*** so higher versions should work fine.
Then you will need to execute:
###### *npm install next react react-dom js-cookie*
then:
***npm run build***
then:
***npm run start***

to navigate in the UI you will need to signup first then you can update the profile. (it has basic authentication so you can create multiple user as long as the API is running [as mentioned above])

please make sure the generated site is in http://localhost:3000

if not please update the code in the API and put-in the right link in the Startup.cs > ConfigureServices (function)
